from llama_index import GPTSimpleVectorIndex, PromptHelper, ServiceContext, QuestionAnswerPrompt, LLMPredictor, download_loader
from langchain.llms import OpenAI
from langchain.chains.question_answering import load_qa_chain
from credentials import db_credentials, openai_credentials
import os
from pathlib import Path

# DatabaseReader = download_loader('DatabaseReader')
JSONReader = download_loader("JSONReader")
os.environ["OPENAI_API_KEY"] = openai_credentials['OPENAI_API_KEY']

def construct_and_test_bot(query):
    max_input_size = 4096
    num_outputs = 512
    max_chunk_overlap = 20
    chunk_size_limit = 600

    # define prompt setting
    prompt_helper = PromptHelper(max_input_size, num_outputs, max_chunk_overlap, chunk_size_limit=chunk_size_limit)
    
    qa_prompt_template = (
        "Kamu adalah pegawai dari Alterstay di bidang reservasi properti/villa yang menjawab pertanyaan customer menggunakan bahasa Indonesia.\n"
        "Alterstay adalah sebuah ekosistem keramahan digital dengan tujuan utama untuk memaksimalkan pengembalian investasi bisnis akomodasi, dengan menyediakan serangkaian produk dan layanan digital yang membantu pemilik bisnis akomodasi, operator, atau investor, mencapai tujuan profitabilitas mereka, melalui integrasi, transparansi, efisiensi, dan otomatisasi.\n"
        "Alterstay adalah sebuah startup teknologi keramahan yang bercita-cita menjadi solusi keramahan all-around yang terdepan. Alterstay memberdayakan pemilik properti untuk mengelola propertinya secara efisien dan terpadu melalui layanan teknologi dan skalabilitas.\n"
        "Alterstay berusaha untuk memberikan pengalaman tamu yang mudah dengan properti terkurasi dan fasilitas standar dengan harga terjangkau. Alterstay berlokasi di Jalan Sunset Road, Sunset Bureau 88, Kuta, Bali 80361, ID.\n"
        "Website Alterstay adalah https://alterstay.co. Alterstay didirikan pada tahun 2022. Alterstay mengelola dan memiliki beberapa properti/villa.\n" 
        "Berikut adalah daftar properti/villa yang dikelola oleh Alterstay:\n"
        "{context_str}"
        "Jangan menyebutkan sesuatu yang tidak ada di informasi ini.\n"
        "Berdasarkan informasi ini, jawablah pertanyaan berikut menggunakan bahasa indonesia: {query_str}\n"
    )
    
    #structure prompt to get the best result
    qa_prompt = QuestionAnswerPrompt(qa_prompt_template)

    #object that use davinci003 (gpt3) model that will receive the prompt
    llm_predictor = LLMPredictor(llm=OpenAI(temperature=0.5, model_name="text-davinci-003", max_tokens=num_outputs))
    
    #merge gpt3 object and prompt setting as a context
    service_context = ServiceContext.from_defaults(llm_predictor=llm_predictor, prompt_helper=prompt_helper)
    
    #perform query, transform the result to a document format for gpt-index
    # documents_production = reader.load_data(query=query)
    loader = JSONReader()
    documents_production = loader.load_data(Path('property.json'))
    
    #create an index to perform the chatbot based on document_production and context
    index = GPTSimpleVectorIndex.from_documents(documents=documents_production,service_context=service_context)
    # index.save_to_disk('index.json')
    history = ""
    #testing the bot
    while True:
        prompt = history + "\n" + input("Saya: ")
        if prompt == 'quit':
            break
        response = index.query(prompt+"\nAlterstay: ", response_mode="default",text_qa_template=qa_prompt)
        history = "".join((history,str(response)))
        print(f"Alterstay: {response} \n")


# reader = DatabaseReader(
#     scheme = "postgresql", # Database Scheme
#     host = "localhost", # Database Host
#     port = "5432", # Database Port
#     user = "postgres", # Database User
#     password = "bram083", # Database Password
#     dbname = "postgres", # Database Name
# )

query = f"""
SELECT
    property_name as nama_properti, property_description as deskripsi_properti, property_address as alamat_properti
FROM public.property
"""

construct_and_test_bot(query)

